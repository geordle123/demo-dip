# Setup

Layered architecture MvC(tiny v on purpose) setup with dependency inversion and module scopes.

Missing dependencies in modules are detected and reported at start-up. There is one single type assertion left in `src/modules/storage/key-value-storage-factory.ts`, this is IO logic and runtime assertable.  

Run with `yarn run start:dev`. Swagger-UI is available at `http://localhost:3000/api/`