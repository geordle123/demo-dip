import { Module } from '@nestjs/common';
import { InvoicesModule } from './modules/business/invoice/invoices.module';
import { StorageModule } from './modules/storage/storage.module';

@Module({
  imports: [InvoicesModule, StorageModule],
})
export class AppModule {}
