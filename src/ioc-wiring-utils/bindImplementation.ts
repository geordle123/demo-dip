import { Abstract, Type } from '@nestjs/common';
import { ClassProvider, IClassProvider } from './classProvider';

export const bindImplementation = <
  TInterface extends Abstract<any>,
  TImplementation extends Omit<TInterface, 'new'> & Type
>(
  providedType: TInterface,
  implementation: TImplementation,
): IClassProvider => {
  return new ClassProvider(providedType, implementation);
};
