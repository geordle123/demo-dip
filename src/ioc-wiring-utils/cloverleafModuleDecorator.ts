import { Module, ModuleMetadata } from '@nestjs/common';
import { IClassProvider } from './classProvider';

interface ClassProviderFacet {
  providers?: IClassProvider[];
}
type CustomModuleMetadata = Pick<
  ModuleMetadata,
  'exports' | 'imports' | 'controllers'
> &
  ClassProviderFacet;

export const CloverleafModule = (
  metadata: CustomModuleMetadata,
): ClassDecorator => {
  const providers = metadata.providers?.map((provider) => provider.get());

  const adaptedMetadata: ModuleMetadata = {
    controllers: metadata.controllers,
    imports: metadata.imports,
    exports: metadata.exports,
    providers: providers,
  };

  return Module(adaptedMetadata);
};
