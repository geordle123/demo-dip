import { Type } from '@nestjs/common/interfaces/type.interface';

export const register = (type: Type) => {
  return { get: () => type };
};
