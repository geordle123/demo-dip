import { Abstract, Type } from '@nestjs/common';
import { Provider } from '@nestjs/common/interfaces/modules/provider.interface';

export interface IClassProvider {
  get: () => Provider;
}

export class ClassProvider<
  TInterface extends Abstract<any>,
  TImplementation extends Omit<TInterface, 'new'> & Type
> implements IClassProvider {
  constructor(private provide: TInterface, private useClass: TImplementation) {}

  get = () => {
    return { provide: this.provide, useClass: this.useClass };
  };
}
