import {
  IKeyValueStorageFactory,
  KeyValueStorageFactory,
} from './key-value-storage-factory';
import { CloverleafModule } from '../../ioc-wiring-utils/cloverleafModuleDecorator';
import { bindImplementation } from '../../ioc-wiring-utils/bindImplementation';

@CloverleafModule({
  providers: [
    bindImplementation(IKeyValueStorageFactory, KeyValueStorageFactory),
  ],
  exports: [IKeyValueStorageFactory],
})
export class StorageModule {}
