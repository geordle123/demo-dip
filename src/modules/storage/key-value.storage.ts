import { Entity } from '../business/entity';
import { IKeyValueStorage } from './key-value.storage.interface';

export class KeyValueStorage<TEntity extends Entity>
  implements IKeyValueStorage<TEntity> {
  private readonly store: Record<Entity['id'], TEntity> = {};

  get = (id: string): TEntity => this.store[id];

  set = (id: string, entity: TEntity) => {
    this.store[id] = entity;
  };

  list = (): TEntity[] => Object.values(this.store);
}
