import { Entity } from '../business/entity';

export interface IKeyValueStorage<TEntity extends Entity> {
  get(id: string): TEntity;

  set(id: string, entity: TEntity): void;

  list(): TEntity[];
}
