import { Injectable, Type } from '@nestjs/common';
import { KeyValueStorage } from './key-value.storage';
import { Entity } from '../business/entity';
import { IKeyValueStorage } from './key-value.storage.interface';

export abstract class IKeyValueStorageFactory {
  abstract get<EntityType extends Entity>(
    type: Type<EntityType>,
  ): IKeyValueStorage<EntityType>;
}

@Injectable()
export class KeyValueStorageFactory implements IKeyValueStorageFactory {
  private readonly stores = new Map<Type, KeyValueStorage<Entity>>();

  get<EntityType extends Entity>(type: Type<EntityType>) {
    return this.getOrCreateStorage(type);
  }

  private getOrCreateStorage<EntityType extends Entity>(
    type: Type<EntityType>,
  ): IKeyValueStorage<EntityType> /*this is our only point of type assertion. It's not that bad because we control stores*/ {
    const store = this.stores.get(type) as KeyValueStorage<EntityType>;
    if (!store) {
      const newStorage = new KeyValueStorage<EntityType>();
      this.stores.set(type, newStorage);
      return newStorage;
    }
    return store;
  }
}
