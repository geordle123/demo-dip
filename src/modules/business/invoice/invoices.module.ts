import { CloverleafModule } from '../../../ioc-wiring-utils/cloverleafModuleDecorator';
import { InvoicePresentationModule } from './controller/invoice.presentation.module';
import { InvoiceDomainModule } from './domain/invoice.domain.module';

@CloverleafModule({
  imports: [InvoicePresentationModule, InvoiceDomainModule],
})
export class InvoicesModule {}
