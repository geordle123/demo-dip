import { CloverleafModule } from '../../../../ioc-wiring-utils/cloverleafModuleDecorator';
import { InvoiceConverterModule } from './invoice-converter/invoice-converter.module';
import { InvoiceController } from './invoice.controller';
import { InvoiceDomainModule } from '../domain/invoice.domain.module';

@CloverleafModule({
  imports: [InvoiceConverterModule, InvoiceDomainModule],
  controllers: [InvoiceController],
})
export class InvoicePresentationModule {}
