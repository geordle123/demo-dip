export class InvoiceDto {
  id: string;
  description: string;
  total: number;
}
