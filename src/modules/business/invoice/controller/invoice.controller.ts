import { Body, Controller, Get, Param, Put } from '@nestjs/common';
import { InvoiceService } from '../domain/invoice.service';
import { IInvoiceToStringConverter } from './invoice-to-string-converter.interface';
import { InvoiceDto } from './invoice.dto';

@Controller('invoice')
export class InvoiceController {
  constructor(
    private readonly invoiceService: InvoiceService,
    private readonly converter: IInvoiceToStringConverter,
  ) {}

  @Get()
  listInvoices(): InvoiceDto[] {
    return this.invoiceService.list();
  }

  @Put()
  createInvoice(@Body() invoice: InvoiceDto) {
    return this.invoiceService.create(invoice);
  }

  @Get(':id')
  getInvoice(@Param('id') id: string): InvoiceDto {
    return this.invoiceService.get(id);
  }

  @Get('convert/:id')
  getConvertedInvoice(@Param('id') id: string): string {
    const invoice = this.invoiceService.get(id);
    return this.converter.convert(invoice);
  }
}
