import { Injectable } from '@nestjs/common';
import { IInvoiceToStringConverter } from '../invoice-to-string-converter.interface';
import { Invoice } from '../../domain/invoice';

@Injectable()
export class InvoiceConverterService implements IInvoiceToStringConverter {
  convert(invoice: Invoice): string {
    return JSON.stringify(invoice);
  }
}
