import { InvoiceConverterService } from './invoice-converter.service';
import { CloverleafModule } from '../../../../../ioc-wiring-utils/cloverleafModuleDecorator';
import { bindImplementation } from '../../../../../ioc-wiring-utils/bindImplementation';
import { IInvoiceToStringConverter } from '../invoice-to-string-converter.interface';

@CloverleafModule({
  providers: [
    bindImplementation(IInvoiceToStringConverter, InvoiceConverterService),
  ],
  exports: [IInvoiceToStringConverter],
})
export class InvoiceConverterModule {}
