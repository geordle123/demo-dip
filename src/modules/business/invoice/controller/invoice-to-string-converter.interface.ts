import { Invoice } from '../domain/invoice';

export abstract class IInvoiceToStringConverter {
  abstract convert(invoice: Invoice): string;
}
