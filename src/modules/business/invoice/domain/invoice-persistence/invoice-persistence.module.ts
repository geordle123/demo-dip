import { StorageModule } from 'src/modules/storage/storage.module';
import { KeyValueInvoiceStorage } from './invoice-persistence.service';
import { bindImplementation } from '../../../../../ioc-wiring-utils/bindImplementation';
import { IInvoiceStorage } from '../invoice.storage.interface';
import { CloverleafModule } from '../../../../../ioc-wiring-utils/cloverleafModuleDecorator';

@CloverleafModule({
  imports: [StorageModule],
  providers: [bindImplementation(IInvoiceStorage, KeyValueInvoiceStorage)],
  exports: [IInvoiceStorage],
})
export class InvoicePersistenceModule {}
