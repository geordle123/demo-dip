import { Injectable } from '@nestjs/common';
import { IInvoiceStorage } from '../invoice.storage.interface';
import { Invoice } from '../invoice';
import { IKeyValueStorage } from '../../../../storage/key-value.storage.interface';
import { IKeyValueStorageFactory } from '../../../../storage/key-value-storage-factory';

@Injectable()
export class KeyValueInvoiceStorage implements IInvoiceStorage {
  private invoiceStorage: IKeyValueStorage<Invoice>;

  constructor(fileStorageFactory: IKeyValueStorageFactory) {
    this.invoiceStorage = fileStorageFactory.get(Invoice);
    this.set = this.invoiceStorage.set;
    this.get = this.invoiceStorage.get;
    this.list = this.invoiceStorage.list;
  }

  set: (id: string, newInvoice: Invoice) => void;
  get: (id: string) => Invoice | undefined;

  list: () => Invoice[];
}
