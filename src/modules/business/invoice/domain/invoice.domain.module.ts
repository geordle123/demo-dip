import { InvoiceService } from './invoice.service';
import { register } from '../../../../ioc-wiring-utils/register';
import { InvoicePersistenceModule } from './invoice-persistence/invoice-persistence.module';
import { CloverleafModule } from '../../../../ioc-wiring-utils/cloverleafModuleDecorator';

@CloverleafModule({
  imports: [InvoicePersistenceModule],
  providers: [register(InvoiceService)],
  exports: [InvoiceService],
})
export class InvoiceDomainModule {}
