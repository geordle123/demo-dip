import { Invoice } from './invoice';

export abstract class IInvoiceStorage {
  abstract set(ID: string, newInvoice: Invoice);
  abstract get(ID: string): Invoice | undefined;

  abstract list(): Invoice[];
}
