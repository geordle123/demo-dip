import { Injectable, NotFoundException } from '@nestjs/common';
import { Invoice } from './invoice';
import { IInvoiceStorage } from './invoice.storage.interface';

@Injectable()
export class InvoiceService {
  constructor(private readonly invoiceStorage: IInvoiceStorage) {}

  create(newInvoice: Invoice) {
    this.invoiceStorage.set(newInvoice.id, newInvoice);
  }

  get(id: string): Invoice {
    const res = this.invoiceStorage.get(id);
    if (!res) {
      throw new NotFoundException();
    }
    return res;
  }

  list() {
    return this.invoiceStorage.list();
  }
}
