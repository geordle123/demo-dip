import { Entity } from '../../entity';

export class Invoice extends Entity {
  description: string;
  total: number;
}
